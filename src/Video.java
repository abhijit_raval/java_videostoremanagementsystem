
public class Video {

	String video_title;

	int rating;
	int totalRating;
	int quantity_video;
	int total_video;

	public int getTotal_video() {
		return total_video;
	}

	public void setTotal_video(int total_video) {
		this.total_video = total_video;
	}

	public int getTotalRating() {
		return totalRating;
	}

	public void setTotalRating(int totalRating) {
		this.totalRating = totalRating;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	int count = 0;

	public String getVideo_title() {
		return video_title;
	}

	public void setVideo_title(String video_title) {
		this.video_title = video_title;
	}

	public int getQuantity_video() {
		return quantity_video;
	}

	public void setQuantity_video(int quantity_video) {
		this.quantity_video = quantity_video;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
}
