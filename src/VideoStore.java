
import java.util.ArrayList;
import java.util.List;

public class VideoStore {

	List<Video> videos = new ArrayList<Video>(10);
	Video video;

	void addVideo(String video_title, int video_quantity) {
		boolean flag = false;
		for (Video video : videos) {
			if (video_title.equalsIgnoreCase(video.getVideo_title()))
				flag = true;
		}
		if (flag) {
			System.out.println("--------------------------");
			System.out.println("Oops!!!Video is already there,can't add it again");
			System.out.println("--------------------------");
		} else {
			video = new Video();
			video.setVideo_title(video_title);
			video.setQuantity_video(video_quantity);
			video.setTotal_video(video_quantity);
			videos.add(video);
		}

	}

	void checkOut(String video_title) {

		for (Video vide : videos) {
			if (video_title.equalsIgnoreCase(vide.getVideo_title()) && (vide.getTotal_video() > 0)) {

				vide.setTotal_video(vide.getTotal_video() - 1);

			} else if (video_title.equalsIgnoreCase(vide.getVideo_title()) && (vide.getTotal_video() == 0)) {
				System.out.println("--------------------------");
				System.out.println(video_title + " not Available");
				System.out.println("--------------------------");
			}
		}
	}

	void returnVideo(String video_title) {

		for (Video vide : videos) {
			if (video_title.equalsIgnoreCase(vide.getVideo_title())
					&& (vide.getTotal_video() < vide.getQuantity_video())) {
				vide.setTotal_video(vide.getTotal_video() + 1);

			} 
			else if(video_title.equalsIgnoreCase(vide.getVideo_title())
					&& (vide.getTotal_video() == vide.getQuantity_video())) {
				System.out.println("--------------------------");
				System.out.println("You have not rented " + video_title + " video");
				System.out.println("--------------------------");
			}

		}

	}

	void receiveRating(String video_title, int rating) {
		boolean flag = false;
		for (Video vide : videos) {
			if (video_title.equalsIgnoreCase(vide.getVideo_title())) {
				vide.setCount(vide.getCount() + 1);
				vide.setTotalRating(vide.getTotalRating() + rating);
				vide.setRating((vide.getTotalRating() / vide.getCount()));
				flag = true;
			}

		}
		if (!flag) {
			System.out.println("--------------------------");
			System.out.println("No videos found");
			System.out.println("--------------------------");
		}
	}

	void listInventory() {
		System.out.println("--------------------------");
		System.out.println("Video title" + "\t" + "Video rating" + "\t" + "video Avaliable");
		for (Video video : videos) {
			System.out.println(video.getVideo_title() + "\t\t" + video.getRating() + "\t\t" + video.getTotal_video());
		}
		System.out.println("--------------------------");
	}

	boolean checkAvailibility(String video_title) {
		for (Video vide : videos) {
			if (video_title.equalsIgnoreCase(vide.getVideo_title()))
				return true;
		}

		return false;
	}

}
