import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class VideoStoreLauncher {
	public static void main(String[] args) throws IOException {
		String video_title;
		int rating = 0, video_quantity = 0;

		BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));

		boolean isTrue = true;
		VideoStore store = new VideoStore();
		while (isTrue) {
			System.out.println("Enter your choice:");
			System.out.println("1.Add Videos");
			System.out.println("2.Rent out a video");
			System.out.println("3.Return a video");
			System.out.println("4.Give Ratings");
			System.out.println("5.List videos");
			System.out.println("6.Exit");
			int choice = Integer.parseInt(sc.readLine());
			switch (choice) {
			case 1:
				System.out.println("Enter video name:");
				video_title = sc.readLine();
				System.out.println("Enter video Quantity:");
				video_quantity = Integer.parseInt(sc.readLine());
				store.addVideo(video_title, video_quantity);
				store.listInventory();
				break;
			case 2:
				System.out.println("--------------------------");
				store.listInventory();
				System.out.println("Enter video to rent out:");
				video_title = sc.readLine();
				if (store.checkAvailibility(video_title)) {
					store.checkOut(video_title);
					break;
				} else {
					System.out.println("--------------------------");
					System.out.println("Video not Available");
					System.out.println("--------------------------");
					break;
				}
			case 3:
				System.out.println("--------------------------");
				System.out.println("Enter video name to return:");
				System.out.println("--------------------------");
				video_title = sc.readLine();
				if (store.checkAvailibility(video_title)) {
					store.returnVideo(video_title);
					break;
				} else {
					System.out.println("--------------------------");
					System.out.println("Video not Available");
					System.out.println("--------------------------");
					break;
				}
			case 4:
				System.out.println("--------------------------");
				store.listInventory();
				System.out.println("Enter video name to rate:");
				video_title = sc.readLine();
				if (store.checkAvailibility(video_title)) {
					System.out.println("Enter video rating:");
					rating = Integer.parseInt(sc.readLine());
					store.receiveRating(video_title, rating);
					break;
				} else {
					System.out.println("--------------------------");
					System.out.println("Video not Available");
					System.out.println("--------------------------");
					break;
				}
			case 5:
				System.out.println("--------------------------");
				store.listInventory();
				break;
			case 6:
				isTrue = false;
				break;
			default:
				System.out.println("--------------------------");
				System.out.println("Please select appropriate option");
				System.out.println("--------------------------");
				break;
			}
		}
	}
}
